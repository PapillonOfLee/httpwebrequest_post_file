﻿namespace WFSendFile
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_BrowserFile = new System.Windows.Forms.Button();
            this.Tb_FileName = new System.Windows.Forms.TextBox();
            this.TB_Status = new System.Windows.Forms.TextBox();
            this.Btn_Send = new System.Windows.Forms.Button();
            this.ProgBarUpload = new System.Windows.Forms.ProgressBar();
            this.Lb_SpeedText = new System.Windows.Forms.Label();
            this.Lb_ProgressPercent = new System.Windows.Forms.Label();
            this.Lb_ConsumeTime = new System.Windows.Forms.Label();
            this.Btn_CancelSend = new System.Windows.Forms.Button();
            this.Tb_Url = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TB_SendResponseData = new System.Windows.Forms.TextBox();
            this.TB_SendDataUrl = new System.Windows.Forms.TextBox();
            this.Btn_SendData = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Btn_MF_Send = new System.Windows.Forms.Button();
            this.TB_MF_Url = new System.Windows.Forms.TextBox();
            this.TB_MF_RES = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Btn_BrowserFile
            // 
            this.Btn_BrowserFile.Location = new System.Drawing.Point(557, 65);
            this.Btn_BrowserFile.Name = "Btn_BrowserFile";
            this.Btn_BrowserFile.Size = new System.Drawing.Size(75, 23);
            this.Btn_BrowserFile.TabIndex = 0;
            this.Btn_BrowserFile.Text = "Browser";
            this.Btn_BrowserFile.UseVisualStyleBackColor = true;
            this.Btn_BrowserFile.Click += new System.EventHandler(this.Btn_BrowserFile_Click);
            // 
            // Tb_FileName
            // 
            this.Tb_FileName.Location = new System.Drawing.Point(75, 65);
            this.Tb_FileName.Name = "Tb_FileName";
            this.Tb_FileName.Size = new System.Drawing.Size(464, 21);
            this.Tb_FileName.TabIndex = 1;
            // 
            // TB_Status
            // 
            this.TB_Status.Location = new System.Drawing.Point(75, 132);
            this.TB_Status.Name = "TB_Status";
            this.TB_Status.Size = new System.Drawing.Size(557, 21);
            this.TB_Status.TabIndex = 2;
            // 
            // Btn_Send
            // 
            this.Btn_Send.Location = new System.Drawing.Point(75, 99);
            this.Btn_Send.Name = "Btn_Send";
            this.Btn_Send.Size = new System.Drawing.Size(75, 23);
            this.Btn_Send.TabIndex = 3;
            this.Btn_Send.Text = "Send";
            this.Btn_Send.UseVisualStyleBackColor = true;
            this.Btn_Send.Click += new System.EventHandler(this.Btn_Send_Click);
            // 
            // ProgBarUpload
            // 
            this.ProgBarUpload.Location = new System.Drawing.Point(75, 209);
            this.ProgBarUpload.Name = "ProgBarUpload";
            this.ProgBarUpload.Size = new System.Drawing.Size(557, 23);
            this.ProgBarUpload.TabIndex = 4;
            // 
            // Lb_SpeedText
            // 
            this.Lb_SpeedText.AutoSize = true;
            this.Lb_SpeedText.Location = new System.Drawing.Point(93, 174);
            this.Lb_SpeedText.Name = "Lb_SpeedText";
            this.Lb_SpeedText.Size = new System.Drawing.Size(35, 12);
            this.Lb_SpeedText.TabIndex = 5;
            this.Lb_SpeedText.Text = "Speed";
            // 
            // Lb_ProgressPercent
            // 
            this.Lb_ProgressPercent.AutoSize = true;
            this.Lb_ProgressPercent.Location = new System.Drawing.Point(293, 174);
            this.Lb_ProgressPercent.Name = "Lb_ProgressPercent";
            this.Lb_ProgressPercent.Size = new System.Drawing.Size(17, 12);
            this.Lb_ProgressPercent.TabIndex = 6;
            this.Lb_ProgressPercent.Text = "0%";
            // 
            // Lb_ConsumeTime
            // 
            this.Lb_ConsumeTime.AutoSize = true;
            this.Lb_ConsumeTime.Location = new System.Drawing.Point(415, 174);
            this.Lb_ConsumeTime.Name = "Lb_ConsumeTime";
            this.Lb_ConsumeTime.Size = new System.Drawing.Size(29, 12);
            this.Lb_ConsumeTime.TabIndex = 7;
            this.Lb_ConsumeTime.Text = "Time";
            // 
            // Btn_CancelSend
            // 
            this.Btn_CancelSend.Location = new System.Drawing.Point(202, 99);
            this.Btn_CancelSend.Name = "Btn_CancelSend";
            this.Btn_CancelSend.Size = new System.Drawing.Size(75, 23);
            this.Btn_CancelSend.TabIndex = 8;
            this.Btn_CancelSend.Text = "Cancel";
            this.Btn_CancelSend.UseVisualStyleBackColor = true;
            this.Btn_CancelSend.Click += new System.EventHandler(this.Btn_CancelSend_Click);
            // 
            // Tb_Url
            // 
            this.Tb_Url.Location = new System.Drawing.Point(75, 29);
            this.Tb_Url.Name = "Tb_Url";
            this.Tb_Url.Size = new System.Drawing.Size(464, 21);
            this.Tb_Url.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(583, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "Url";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TB_SendResponseData);
            this.groupBox1.Controls.Add(this.TB_SendDataUrl);
            this.groupBox1.Controls.Add(this.Btn_SendData);
            this.groupBox1.Location = new System.Drawing.Point(42, 252);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(641, 110);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SendDataTest";
            // 
            // TB_SendResponseData
            // 
            this.TB_SendResponseData.Location = new System.Drawing.Point(33, 80);
            this.TB_SendResponseData.Name = "TB_SendResponseData";
            this.TB_SendResponseData.Size = new System.Drawing.Size(521, 21);
            this.TB_SendResponseData.TabIndex = 2;
            // 
            // TB_SendDataUrl
            // 
            this.TB_SendDataUrl.Location = new System.Drawing.Point(33, 42);
            this.TB_SendDataUrl.Name = "TB_SendDataUrl";
            this.TB_SendDataUrl.Size = new System.Drawing.Size(521, 21);
            this.TB_SendDataUrl.TabIndex = 1;
            // 
            // Btn_SendData
            // 
            this.Btn_SendData.Location = new System.Drawing.Point(560, 40);
            this.Btn_SendData.Name = "Btn_SendData";
            this.Btn_SendData.Size = new System.Drawing.Size(75, 23);
            this.Btn_SendData.TabIndex = 0;
            this.Btn_SendData.Text = "Send";
            this.Btn_SendData.UseVisualStyleBackColor = true;
            this.Btn_SendData.Click += new System.EventHandler(this.Btn_SendData_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TB_MF_RES);
            this.groupBox2.Controls.Add(this.TB_MF_Url);
            this.groupBox2.Controls.Add(this.Btn_MF_Send);
            this.groupBox2.Location = new System.Drawing.Point(42, 390);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(641, 166);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "mulitipart-formdata";
            // 
            // Btn_MF_Send
            // 
            this.Btn_MF_Send.Location = new System.Drawing.Point(560, 34);
            this.Btn_MF_Send.Name = "Btn_MF_Send";
            this.Btn_MF_Send.Size = new System.Drawing.Size(75, 23);
            this.Btn_MF_Send.TabIndex = 0;
            this.Btn_MF_Send.Text = "Send";
            this.Btn_MF_Send.UseVisualStyleBackColor = true;
            this.Btn_MF_Send.Click += new System.EventHandler(this.Btn_MF_Send_Click);
            // 
            // TB_MF_Url
            // 
            this.TB_MF_Url.Location = new System.Drawing.Point(8, 36);
            this.TB_MF_Url.Name = "TB_MF_Url";
            this.TB_MF_Url.Size = new System.Drawing.Size(546, 21);
            this.TB_MF_Url.TabIndex = 1;
            // 
            // TB_MF_RES
            // 
            this.TB_MF_RES.Location = new System.Drawing.Point(8, 79);
            this.TB_MF_RES.Name = "TB_MF_RES";
            this.TB_MF_RES.Size = new System.Drawing.Size(546, 21);
            this.TB_MF_RES.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 568);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Tb_Url);
            this.Controls.Add(this.Btn_CancelSend);
            this.Controls.Add(this.Lb_ConsumeTime);
            this.Controls.Add(this.Lb_ProgressPercent);
            this.Controls.Add(this.Lb_SpeedText);
            this.Controls.Add(this.ProgBarUpload);
            this.Controls.Add(this.Btn_Send);
            this.Controls.Add(this.TB_Status);
            this.Controls.Add(this.Tb_FileName);
            this.Controls.Add(this.Btn_BrowserFile);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_BrowserFile;
        private System.Windows.Forms.TextBox Tb_FileName;
        private System.Windows.Forms.TextBox TB_Status;
        private System.Windows.Forms.Button Btn_Send;
        private System.Windows.Forms.ProgressBar ProgBarUpload;
        private System.Windows.Forms.Label Lb_SpeedText;
        private System.Windows.Forms.Label Lb_ProgressPercent;
        private System.Windows.Forms.Label Lb_ConsumeTime;
        private System.Windows.Forms.Button Btn_CancelSend;
        private System.Windows.Forms.TextBox Tb_Url;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TB_SendDataUrl;
        private System.Windows.Forms.Button Btn_SendData;
        private System.Windows.Forms.TextBox TB_SendResponseData;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Btn_MF_Send;
        private System.Windows.Forms.TextBox TB_MF_Url;
        private System.Windows.Forms.TextBox TB_MF_RES;
    }
}

